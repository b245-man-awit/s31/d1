// use the "require" directive to load Node.js modules
// A "module" is a software component or part of a program that contains one or more routines.
// "http module" let us Node .js transfer data using the Hyper Text Transfer Protocol.

let http = require("http");

http.createServer(function (request, response) {

    // use the writeHead() method to:
    // Set a status code for the response
    // set the content-type of the response as a plain text message
    response.writeHead(200, {'Content-Type': 'text/plain'});    
    response.end("hello, B245!");

}).listen(4000);    // 4000 kasi ung common use

console.log('Server running at localhost: 4000');